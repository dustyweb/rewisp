(define wisp-hello-world
  "\
define hello
  string-append \"hello! \" \"world!\"
")

(define wisp-hello-world2
  "\
define hello
  string-append : \"hello! \" \"world!\"
")


(define wisp-fac-underscores
  "\
define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial {n - 1}")

(define wisp-fac-noinfix2
  "\
define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial : - n 1")

(define wisp-fac-noinfix-repeat
  "\
define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial : - n 1

define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial : - n 1")


(define wisp-fac-no-underscores
  "\
define : factorial n
    if : zero? n
       . 1
       * n : factorial {n - 1}")

(define multiple-exprs
  "\
define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial {n - 1}

display : factorial 5 
newline")
