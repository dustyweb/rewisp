;;; Copyright 2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (rewisp)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (system syntax)
  #:export (read-wisp-lines
            read-wisp-sexp

            <line>
            line?
            make-line line-indent line-args

            <DOT>
            DOT?
            make-DOT))

;; Used for interpreting wisp in "line form"
(define-record-type <line>
  (make-line indent args)
  line?
  (indent line-indent)
  (args line-args))

(define-record-type <DOT>
  (make-DOT source-loc)
  DOT?
  (source-loc DOT-source-loc))

(define-record-type <COMMENT>
  (make-COMMENT source-loc text)
  COMMENT?
  (text COMMENT-text)
  (source-loc COMMENT-source-loc))

(define-record-type <PARENTHETICAL>
  (make-PARENTHETICAL source-loc datum)
  PARENTHETICAL?
  (source-loc PARENTHETICAL-source-loc)
  (datum PARENTHETICAL-datum))

(define-record-type <CURLY>
  (make-CURLY source-loc datum)
  CURLY?
  (source-loc CURLY-source-loc)
  (datum CURLY-datum))

(define (keyword-in-syntax? obj)
  (and (syntax? obj)
       (keyword? (syntax->datum obj))))

(define (token-char? char)
  (match char
    ((? eof-object?)
     #f)
    ((or (? char-alphabetic?)
         (? char-numeric?)
         #\! #\$ #\% #\^ #\& #\* #\- #\_ #\. #\- #\_ #\$ #\< #\>
         #\? #\+ #\= #\~ #\:)
     #t)
    (_ #f)))

(define (read-line ip keep-structure?)
  (define filename
    (port-filename ip))
  ;; (define (get-location)
  ;;   (make-source-location (port-filename ip)
  ;;                         (port-line ip)
  ;;                         (port-column ip)
  ;;                         (false-if-exception (ftell ip))
  ;;                         #f))

  ;; These two procedures and macro simplify marking the syntax
  ;; location of things we parse
  (define (get-source-loc)
    (vector filename
            (port-line ip)
            (port-column ip)))
  (define (get-locifier)
    (define source-loc (get-source-loc))
    (define (locify val)
      (datum->syntax #f val #:source source-loc))
    locify)
  (define-syntax-rule (locify body ...)
    (let ((locifier (get-locifier)))
      (locifier (begin body ...))))

  (define (read-token)
    (let lp ()
      (if (token-char? (peek-char ip))
          (cons (read-char ip)
                (lp))
          '())))

  (define (raise-unexpected char)
    (error (format #f "Unexpected character on line ~a col ~a: ~a"
                   (port-line ip) (port-column ip)
                   char)))

  (define (eat-whitespace-and-underscores)
    (let lp ((count 0))
      (match (peek-char ip)
        ((or (? eof-object?)
             #\newline)
         count)
        (#\_
         (read-char ip)
         (lp (1+ count)))
        (_ (eat-whitespace count)))))

  (define* (eat-whitespace #:optional (count 0))
    (match (peek-char ip)
      ((or (? eof-object?)
           #\newline)
       count)
      ((? char-whitespace?)
       (read-char ip)
       (eat-whitespace (1+ count)))
      (_ count)))
  ;; first eat the indentation up until we know
  ;; how much this line of indentation has
  (define indent-level
    (eat-whitespace-and-underscores))

  (define (read-string)
    (read-char ip)
    (list->string
     (let lp ()
       (match (read-char ip)
         (#\" '())
         ;; as much backquoting as I feel like right now :P
         (#\\
          (match (read-char ip)
            (#\newline
             (lp))
            (#\n (cons #\newline
                       (lp)))
            (#\t (cons #\tab
                       (lp)))))
         (char
          (cons char (lp)))))))

  (define* (read-next #:optional first?)
    (eat-whitespace)
    (match (peek-char ip)
      ((? eof-object?)
       '())
      (#\;
       (let ((comment (make-COMMENT
                       (get-source-loc)
                       (begin
                         (read-char ip)
                         (list->string
                          (let lp ()
                            (match (read-char ip)
                              ((or #\newline (? eof-object?))
                               '())
                              (char (cons char (lp))))))))))
         ;; maybe throw it away!
         (if keep-structure?
             (list comment)
             '())))
      (#\newline
       (read-char ip)
       '())
      ;; (#\.
      ;;  (read-char ip)
       
      ;;  )
      ;; quote
      (#\'
       (cons (locify (read-char ip)
                     'quote)
             (read-next)))
      ;; strings
      (#\"
       (cons (locify (read-string))
             (read-next)))
      (#\(
       (cons (if keep-structure?
                 (make-PARENTHETICAL (get-source-loc)
                                     (read-syntax ip))
                 (read-syntax ip))
             (read-next)))
      (#\(
       (cons (if keep-structure?
                 (make-CURLY (get-source-loc)
                             (read-syntax ip))
                 (read-syntax ip))
             (read-next)))
      (#\:
       (let ((source-loc (get-source-loc)))
         (match (read-token)
           ((#\:)  ; just the colon
            (cons (read-next)
                  '()))
           (token
            (let ((sym (string->symbol
                        (list->string token))))
              (cons (datum->syntax #f sym #:source source-loc)
                    (read-next)))))))
      (#\.
       (let ((source-loc (get-source-loc)))
         (match (read-token)
           ((#\.)  ; just the dot
            (cons (make-DOT source-loc)
                  (read-next)))
           (token
            (let ((sym (string->symbol
                        (list->string token))))
              (cons (datum->syntax #f sym #:source source-loc)
                    (read-next)))))))
      (#\#
       (let ((locifier (get-locifier)))
         (read-char ip)
         (match (peek-char ip)
           (#\(
            (cons (locifier (list->vector (read ip)))
                  (read-next)))
           (_
            (match (read-token)
              ;; keyword
              ((#\: string-chars ...)
               (cons (locifier
                      (symbol->keyword (string->symbol (list->string string-chars))))
                     (read-next)))
              (other-hash-expr
               (error "Unexpected hash-expression on line ~a col ~a: ~a"
                      (port-line ip) (port-column ip)
                      (list->string (cons #\# other-hash-expr)))))))))
      ;; it's an atom of some type
      ((? token-char?)
       (let ((locifier (get-locifier))
             (token (read-token)))
         (cons (match token
                 ;; integers
                 (((? char-numeric?) ...)
                  (locifier
                   (string->number
                    (list->string token))))
                 ;; negative integers
                 ((#\- (? char-numeric?) ..+)
                  (locifier
                   (* -1
                      (string->number
                       (list->string token)))))
                 ;; symbols
                 (_
                  (locifier
                   (string->symbol (list->string token)))))
               (read-next))))
      (other-char
       (raise-unexpected other-char))))

  (make-line indent-level
             (read-next #t)))

(define* (read-wisp-lines ip #:key keep-structure?)
  (let lp ()
    (if (eof-object? (peek-char ip))
        '()
        (let ((new-line (read-line ip keep-structure?)))
          (if (null? (line-args new-line))
              (cons #f (lp))
              (cons new-line (lp)))))))

(define (gather-children our-indent get-lines claim-line!)
  (let lp ()
    (match (get-lines)
      ('() '())
      ((nl rest ...)
       (cond
        ;; It's #f, so we just pull it in as.
        ;; Represents an empty line.
        ((not nl)
         (claim-line!)
         (cons #f (lp)))
        ;; we only grab lines that are bigger than us
        ;; so this would mean we're done
        ((and our-indent (not (> (line-indent nl) our-indent)))
         '())
        ;; Otherwise, pull it in...
        (else
         (match (line-args nl)
           ;; not a subexpression
           (((or (? DOT?) (? keyword-in-syntax?)) rest ...)
            (claim-line!)
            (cons nl (lp)))
           (_args
            (claim-line!)
            (cons (cons nl
                        (gather-children (line-indent nl)
                                         get-lines
                                         claim-line!))
                  (lp))))))))))

(define (parse-lines lines)
  (define (get-lines)
    lines)
  (define (claim-line!)
    (set! lines (cdr lines)))
  (gather-children #f get-lines claim-line!))

(define (strip-bare obj)
  (match obj
    ('() '())
    ;; skip out comments
    (((? COMMENT?) . rest)
     (strip-bare rest))
    ;; strip list sub-components
    ((item . rest)
     (cons (strip-bare item)
           (strip-bare rest)))
    ;; individual things to strip
    ((? syntax?)
     (strip-bare (syntax->datum obj)))
    ((? PARENTHETICAL?)
     (strip-bare (PARENTHETICAL-datum obj)))
    ((? CURLY?)
     (strip-bare (CURLY-datum obj)))
    (_ obj)))

(define (unpack-line-tree lst)
  (apply append
         (map
          (match-lambda
            (#f '())
            ((? line? line)
             (match (line-args line)
               (((? DOT?) rest ...)
                rest)
               (args args)))
            ((new-lst ...)
             (list (unpack-line-tree new-lst))))
          lst)))

(define (parse-lines->sexp lines)
  (define line-tree (parse-lines lines))
  (strip-bare (unpack-line-tree line-tree)))

(define wisp-fac-noinfix-repeat
  "\
define : factorial n  ; foo
__  if : zero? n
____   . 1
____   * n : factorial : - n 1

define : factorial n
__  if : zero? n
____   . 1
____   * n : factorial : - n 1")


;; (parse-lines->sexp (call-with-input-string wisp-fac-noinfix-repeat read-wisp-lines))
